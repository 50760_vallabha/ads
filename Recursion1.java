/*
class Recursion1{
    static void series(int a){
        if(a>=10){
            System.out.println(a);
        }
        else{
             
            series(a+1);
            System.out.println(a);
        }

    }

    public static void main(String args[]){
        
        series(1);
    }
}
*/

/*
class Recursion1{

    static void series(int a){ //s(1)-s(2)-s(3) -s(4) --------s(9)- s(10)
        if(a>=10){              //1 - 2 - 3
                System.out.println(a);
        }
        else{
            System.out.println(a);
            series(a+1);
            System.out.println(a);
             
            
        }
    }

    public static void main(String args[]){
        series(1);
    }
}


*/

/*
class Recursion1{
    static int sum(int a){
      if(a<=0){
        return a;
      }  
      else{
          
           return  a + sum(a-1);
      }
    }

    public static void main(String args[]){
        //sum(50);
        System.out.println(sum(50));
    }
}

*/

/*
class Recursion1{
    static int fact(int a){
        if(a<=1){
            return a;
        }
        else{
            return a*fact(a-1);
        }
    }

    public static void main(String args[]){
        System.out.println(fact(21));
    }
}

*/

class Recursion1{
   static int x = 0, y=1, z=0;
    static void fibo(int a){
        
        if(a>=0){
            
            
            z=x+y;
            x = y;
            y=z;

            System.out.println(z);
             

            fibo(a-1);
        }
    }

    public static void main(String args[]){
        fibo(10);
    }
}